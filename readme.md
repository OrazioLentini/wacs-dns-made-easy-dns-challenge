# Lets Encrypt - WACS

### DNSMadeEasy - Challenge

Uses DNSMadeEasy api to create a txt record to validate domain. 

| Action | Command |
| ------ | ------ |
| Create | ./create.ps1 -RecordName '{RecordName}' -TxtValue '{Token}' -DMEKey '{APIKEY}' -DMESecret '{SECRETKEY}' |
| Delete | ./delete.ps1 -RecordName '{RecordName}' -TxtValue '{Token}' -DMEKey '{APIKEY}' -DMESecret '{SECRETKEY}' |

### Resources

| Action | Command |
| ------ | ------ |
| WIN-ACME DNS Validation | https://www.win-acme.com/reference/plugins/validation/dns/script |
| DNSMadeEasy Powershell | https://github.com/rmbolger/Posh-ACME/blob/master/Posh-ACME/DnsPlugins/DMEasy.ps1 |
