param(
    [string]$RecordName,
    [string]$TxtValue,
    [string]$DMEKey,
    [string]$DMESecret
)

[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12

$apiBase = 'https://api.dnsmadeeasy.com/V2.0/dns/managed'

# START CREATE HEADER #
$secBytes = [Text.Encoding]::UTF8.GetBytes($DMESecret)
$hmac = New-Object Security.Cryptography.HMACSHA1($secBytes,$true)
$reqDate = [System.DateTimeOffset]::Now.ToString('r')
$dateBytes = [Text.Encoding]::UTF8.GetBytes($reqDate)
$dateHash = [BitConverter]::ToString($hmac.ComputeHash($dateBytes)).Replace('-','').ToLower()
$header = @{
    'x-dnsme-apiKey'      = $DMEKey;
    'x-dnsme-requestDate' = $reqDate;
    'x-dnsme-hmac'        = $dateHash;
}
$auth = $header
# END CREATE HEADER #

Write-Host "$RecordName"
Write-Host "Attempting to find hosted zone for $RecordName"
$header | Out-String | Write-Host
try {
    $response = Invoke-RestMethod $ApiBase -Headers $auth -ContentType 'application/json'
    $zones = $response.data
} catch { throw }

Write-Host $RecordName
$pieces = $RecordName.Split('.')
$zoneID = ""
$zoneName = ""
for ($i=1; $i -lt ($pieces.Count-1); $i++) {
    $zoneTest = "$( $pieces[$i..($pieces.Count-1)] -join '.' )"
    Write-Host $zoneTest

    if ($zoneTest -in $zones.name) {
        $zone = $zones | Where-Object { $_.name -eq $zoneTest }
        $zoneID=  $zone.id
        $zoneName = $zone.name
        break    
    }
}

if ($zoneID -eq "") { 
    throw "Unable to find DME hosted zone for $RecordName"
}

if ($zoneName -eq "") { 
    throw "Unable to find DME hosted zone for $RecordName"
}

$recShort = $RecordName -ireplace [regex]::Escape(".$zoneName"), [string]::Empty
$recRoot = "$apiBase/$zoneID/records"
Write-Host $recShort

try {
    $response = Invoke-RestMethod "$($recRoot)?recordName=$recShort&type=TXT" `
        -Headers $auth -ContentType 'application/json'
} catch { throw }

Write-Host $response
# check if our value is already in there
if ($response.totalRecords -gt 0) {
    if ("`"$TxtValue`"" -in $response.data.value) {
        Write-Host "Record $RecordName already contains $TxtValue. Nothing to do."
        continue
    }
}
else {
    try {
        $bodyJson = @{name=$recShort;value="`"$TxtValue`"";type='TXT';ttl=10} | ConvertTo-Json -Compress
        Write-Verbose "Creating $RecordName with value $TxtValue"
        Invoke-RestMethod $recRoot -Method Post -Body $bodyJson -Headers $auth `
            -ContentType 'application/json' | Out-Null
    } catch { throw }
}